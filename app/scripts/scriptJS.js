$(document).ready(function() {
  if ($('.products-carusel ul li').length == 1) {
    var liCounts = 1
  } else if ($('.products-carusel ul li').length == 2) {
    var liCounts = 3;
    $('.products-carusel ul').append($('.products-carusel ul').html());

  } else if ($('.products-carusel ul li').length > 2) {
    var liCounts = 3;
    $('.products-carusel ul').append($('.products-carusel ul').html());
  }
  $('.products-carusel ul').jcarousel({
    itemLoadCallback: {
      onBeforeAnimation: myanimationStepCallback
    },
    visible: liCounts,
    scroll: 1,
    animation: 1000,
    start: 1,
    wrap: 'circular'
  });

  $('.jcarousel-prev, .jcarousel-next').on('mouseover mouseleave', function(e) {
    e.preventDefault();
    $(this).toggleClass('hover');
  });


});

function myanimationStepCallback(carousel) {
  var idx = carousel.first;
  $('.products-carusel').addClass('done')
  $('.products-carusel ul li').css('overflow', 'hidden').attr('id', '');
  $('.products-carusel ul li .shadow').animate({
    'opacity': 0.2
  }, 250);

  //setTimeout(function(){
  $('.products-carusel ul li:[jcarouselindex=\'' + (idx + 1) + '\']').attr('id', 'active-slide-element');
  $('.products-carusel ul li:[jcarouselindex=\'' + (idx + 1) + '\'] .front').each(function() {
    $(this).attr('src', $(this).attr('srcC'));
    var self = this;
    $('.products-carusel .description').animate({
      'opacity': '0'
    }, 200, function() {
      $('.products-carusel .description .name span').html($(self).attr('alt'));
      $('.products-carusel .description .name b').html($(self).attr('title'));
      if ($(self).attr('type')) {
        $('.products-carusel .description em span').css('display', 'block').html($(self).attr('type'));
      } else {
        $('.products-carusel .description em span').css('display', 'none');
      }
      $(this).animate({
        'opacity': '1'
      }, 200);
    });
  });
  setTimeout(function() {
    $('.products-carusel ul li:[jcarouselindex=\'' + (idx) + '\'] .front').each(function() {
      if ($(this).attr('srcL') != '') {
        $(this).attr('src', $(this).attr('srcL'));
      } else {
        $(this).attr('src', $(this).attr('srcC'));
      }
    });
  }, 500);
  $('.products-carusel ul li:[jcarouselindex=\'' + (idx - 1) + '\'] .front').each(function() {
    if ($(this).attr('srcL') != '') {
      $(this).attr('src', $(this).attr('srcL'));
    } else {
      $(this).attr('src', $(this).attr('srcC'));
    }
  });

  $('.products-carusel ul li:[jcarouselindex=\'' + (idx + 3) + '\'] .front').each(function() {
    $(this).attr('src', $(this).attr('srcR'));
  });
  $('.products-carusel ul li:[jcarouselindex=\'' + (idx + 2) + '\'] .front').each(function() {
    if ($(this).attr('srcR') != '') {
      $(this).attr('src', $(this).attr('srcR'));
    } else {
      $(this).attr('src', $(this).attr('srcC'));
    }
  });
  $('.products-carusel ul li .shadow').animate({
    'opacity': 1
  }, 250, function() {

  });
  //}, 250);

  $('.products-carusel ul li:[jcarouselindex=\'' + (idx + 1) + '\']').css('overflow', 'visible').removeClass('s-l, s-r');
  $('.products-carusel ul li:[jcarouselindex=\'' + (idx) + '\']').css('overflow', 'visible').removeClass('s-r').addClass('s-l');
  $('.products-carusel ul li:[jcarouselindex=\'' + (idx + 2) + '\']').css('overflow', 'visible').removeClass('s-l').addClass('s-r');


    console.log("start");
  	if($(window).width() > '500' && $(window).width() < '2000'){
  			console.log("resize"  + $(window).width());
  		if ($(".products-carusel").is(".cigarettes")) {
  			$(".products-carusel ul li:[jcarouselindex='"+(idx+1)+"'] .front").animate({"height":"320px", marginTop:0}, 500);
  			$(".products-carusel ul li:[jcarouselindex='"+(idx)+"'] .front").animate({"height":"240px", marginTop:'20px'}, 500);
  			$(".products-carusel ul li:[jcarouselindex='"+(idx+2)+"'] .front").animate({"height":"240px", marginTop:'20px'}, 500);
  		}
  	}
    //
    if($(window).width() > '520' && $(window).width() < '660'){
        console.log("resize"  + $(window).width());
        	$(".jcarousel-next,.jcarousel-prev").css( "marginTop", "-60px" );
      if ($(".products-carusel").is(".cigarettes")) {
        $(".products-carusel ul li:[jcarouselindex='"+(idx+1)+"'] .front").animate({"height":"240px", marginTop:0}, 500);
        $(".products-carusel ul li:[jcarouselindex='"+(idx)+"'] .front").animate({"height":"180px", marginTop:'20px'}, 500);
        $(".products-carusel ul li:[jcarouselindex='"+(idx+2)+"'] .front").animate({"height":"180px", marginTop:'20px'}, 500);
      }
    }
    if($(window).width() > '480' && $(window).width() < '520'){
        console.log("resize"  + $(window).width());
          $(".jcarousel-next,.jcarousel-prev").css( "marginTop", "-40px" );
      if ($(".products-carusel").is(".cigarettes")) {
        $(".products-carusel ul li:[jcarouselindex='"+(idx+1)+"'] .front").animate({"height":"170px", marginTop:0}, 500);
        $(".products-carusel ul li:[jcarouselindex='"+(idx)+"'] .front").animate({"height":"120px", marginTop:'20px'}, 500);
        $(".products-carusel ul li:[jcarouselindex='"+(idx+2)+"'] .front").animate({"height":"120px", marginTop:'20px'}, 500);
      }
    }
    if($(window).width() > '380' && $(window).width() < '480'){
        console.log("resize"  + $(window).width());
          $(".jcarousel-next,.jcarousel-prev").css( "marginTop", "-30px" );
      if ($(".products-carusel").is(".cigarettes")) {
        $(".products-carusel ul li:[jcarouselindex='"+(idx+1)+"'] .front").animate({"height":"200px", marginTop:0}, 500);
        $(".products-carusel ul li:[jcarouselindex='"+(idx)+"'] .front").animate({"height":"155px", marginTop:'20px'}, 500);
        $(".products-carusel ul li:[jcarouselindex='"+(idx+2)+"'] .front").animate({"height":"155px", marginTop:'20px'}, 500);
      }
    }
    if($(window).width() > '350' && $(window).width() < '380'){
        console.log("resize"  + $(window).width());
          $(".jcarousel-next,.jcarousel-prev").css( "marginTop", "-30px" );
      if ($(".products-carusel").is(".cigarettes")) {
        $(".products-carusel ul li:[jcarouselindex='"+(idx+1)+"'] .front").animate({"height":"170px", marginTop:0}, 500);
        $(".products-carusel ul li:[jcarouselindex='"+(idx)+"'] .front").animate({"height":"130px", marginTop:'20px'}, 500);
        $(".products-carusel ul li:[jcarouselindex='"+(idx+2)+"'] .front").animate({"height":"130px", marginTop:'20px'}, 500);
      }
    }
    if($(window).width() > '319' && $(window).width() < '350'){
        console.log("resize"  + $(window).width());
          $(".jcarousel-next,.jcarousel-prev").css( "marginTop", "-40px" );
      if ($(".products-carusel").is(".cigarettes")) {
        $(".products-carusel ul li:[jcarouselindex='"+(idx+1)+"'] .front").animate({"height":"150px", marginTop:0}, 500);
        $(".products-carusel ul li:[jcarouselindex='"+(idx)+"'] .front").animate({"height":"100px", marginTop:'20px'}, 500);
        $(".products-carusel ul li:[jcarouselindex='"+(idx+2)+"'] .front").animate({"height":"100px", marginTop:'20px'}, 500);
      }
    }
}
